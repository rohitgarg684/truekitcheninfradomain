﻿using System.Collections.Generic;

namespace Truekitchen.Infra.Domain.Pagination
{
    public class PagedResponse<T>
    {
        public PagedResponse(IEnumerable<T> data, PageInfo pageInfo)
        {
            Data = data;
            PageInfo = pageInfo;
        }

        public IEnumerable<T> Data { get; private set; }
        public PageInfo PageInfo { get; private set; }
    }
}
