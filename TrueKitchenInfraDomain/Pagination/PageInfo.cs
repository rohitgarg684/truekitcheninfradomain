﻿namespace Truekitchen.Infra.Domain.Pagination
{
    public class PageInfo
    {
        public PageInfo(int? totalCount, PageRequest currentPageRequest)
        {
            TotalCount = totalCount;
            if (totalCount.HasValue)
            {
                NextPageToken = PageRequest.GetNextPage(totalCount.Value, currentPageRequest);
                PreviousPageToken = PageRequest.GetPreviousPage(totalCount.Value, currentPageRequest);
            }
        }

        public bool HasMore {
            get
            {
                return NextPageToken != null;
            }
        }

        public PageRequest NextPageToken {get; private set;}
        public PageRequest PreviousPageToken { get;  private set; } 
        public int? TotalCount { get; private set; }
    }
}