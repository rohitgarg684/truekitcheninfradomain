﻿using Truekitchen.Infra.Domain.Validation;

namespace Truekitchen.Infra.Domain.Pagination
{
    public class PageRequest
    {
        private static int _maxPageSize = 100;
        public PageRequest()
        {
            Asc = true;
            PageSize = _maxPageSize;
            PageNo = 1;
        }

        private int _pageSize;
        public int PageNo { get; set; }
        public int PageSize
        {
            get { return _pageSize; }
            set
            {
                var validationResponse = ValidatePageSize(value);
                if (validationResponse.IsError)
                {
                    throw new ValidationException(validationResponse);
                }
                _pageSize = value;
            }
        }
        public bool Asc { get; set; }

        public int GetSkip()
        {
            return PageSize * (PageNo - 1);
        }

        public int GetTake()
        {
            return PageSize;
        }

        public static ValidationResponse ValidatePageSize(int pageSize)
        {
            var validationResponse = new ValidationResponse();
            if (pageSize > _maxPageSize)
            {
                validationResponse.AddError(new ErrorResponse($"Page size can not be greater than {_maxPageSize}", 295));
                return validationResponse;
            }

            if (pageSize < 1)
            {
                validationResponse.AddError(new ErrorResponse($"Page size is invalid", 296));
                return validationResponse;
            }

            return validationResponse;
        }

        public static PageRequest GetNextPage(int totalRecordCount, PageRequest currentPageRequest)
        {
            var currentRecordCount = currentPageRequest.PageSize * currentPageRequest.PageNo;
            if(totalRecordCount > currentRecordCount)
            {
                return new PageRequest
                {
                    Asc = currentPageRequest.Asc,
                    PageSize = currentPageRequest.PageSize,
                    PageNo = currentPageRequest.PageNo + 1
                };
            }

            return null;
        }

        public static PageRequest GetPreviousPage(int totalRecordCount, PageRequest currentPageRequest)
        {
            var currentRecordCount = currentPageRequest.PageSize * currentPageRequest.PageNo;
            if (totalRecordCount > 0 && currentPageRequest.PageNo > 1)
            {
                return new PageRequest
                {
                    Asc = currentPageRequest.Asc,
                    PageSize = currentPageRequest.PageSize,
                    PageNo = currentPageRequest.PageNo - 1
                };
            }

            return null;
        }
    }
}
