﻿using System;

namespace Truekitchen.Infra.Domain.Validation
{
    public class ValidationException:Exception
    {
        public ValidationException(ValidationResponse validationResponse)
        {
            ValidationResponse = validationResponse;
        }

        public ValidationException(string error, int code)
        {
            ValidationResponse = new ValidationResponse();
            ValidationResponse.AddError(new ErrorResponse(error, code));
        }

        public ValidationResponse ValidationResponse { get; }
    }
}
