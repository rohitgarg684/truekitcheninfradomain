﻿using System.Collections.Generic;
using System.Linq;

namespace Truekitchen.Infra.Domain.Validation
{
    public class ValidationResponse
    {
        public ValidationResponse()
        {
            IsError = false;
        }

        public bool IsError { get; set; }
        public List<ErrorResponse> ErrorResponses { get; set; }

        public void AddError(ErrorResponse errorResponse)
        {
            IsError = true;
            if (ErrorResponses == null)
                ErrorResponses = new List<ErrorResponse>();

            ErrorResponses.Add(errorResponse);
        }

        public void AddErrors(IList<ErrorResponse> errorResponses)
        {
            if (errorResponses == null || !errorResponses.Any())
                return;

            IsError = true;
            if (ErrorResponses == null)
                ErrorResponses = new List<ErrorResponse>();

            ErrorResponses.AddRange(errorResponses);
        }

        public void AddError(int errorCode, string errorMessage)
        {
            AddError(new ErrorResponse(errorMessage, errorCode));
        }

    }
    public class ErrorResponse
    {
        public ErrorResponse(string errorMessage, int errorCode)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
        }
        public int ErrorCode { get; }
        public string ErrorMessage { get; }
    }
}
